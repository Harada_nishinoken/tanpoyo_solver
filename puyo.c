#include<stdio.h>
#include<string.h>

/*盤面入力(配列の入れ方)*/
/*
.
.
.
.
(6)(7)(8)(9)(10)(11)
(0)(1)(2)(3)(4) (5)
*/
/*next入力(配列の入れ方)*/
/*
(0)(1)(2)(3)(4)(5)....
*/

//盤面とNextの構造体
typedef struct{
  int next[78];
  int banmen[78];
}state;

state ini_puyo(char *nex, char *ban);
void print_state(state);
state rensa(state state);
int search_rensa(state);

int main( int argc, char *argv[]){

  int ans=0;
  state p_state;

  printf("%s\n",argv[1]);

  p_state = ini_puyo( argv[1], argv[2]);

  ans = search_rensa( p_state);

  printf("最大連鎖数:[%d]\n",ans);  
}


//初期入力を盤面とNextに反映
state ini_puyo(char *nex, char *ban){

  int next_len = strlen(nex);
  int ban_len = strlen(ban);
  printf("nex[%s]len[%d]\n",nex, next_len);
  printf("ban[%s]len[%d]\n",ban, ban_len);
  state ini_state;
  int i=0;

  for( i=0; i<78; i++){
    if(i<next_len){
      ini_state.next[i] = nex[i]-'0';
    }else{
      ini_state.next[i] = 0;
    }
    if(i<ban_len){
      ini_state.banmen[i] = ban[i]-'0';
    }else{
      ini_state.banmen[i] = 0;
    }
  }

  print_state(ini_state);

  return ini_state;
}


//盤面とNextを出力
void print_state(state print_state){
  int i=0;

  printf("保持ぷよの状況\n");
  for(i=0; i<78; i++){
    printf("[%d]", print_state.next[i]);
    if(i%5==4){
      printf("\n");
    }
  }

  printf("\n盤面情報\n");
  for(i=72; i>=0; i++){
    printf("[%d]", print_state.banmen[i]);
    if(i%6 == 5){
      i -= 12;
      printf("\n");
    }
  }
}

//実際に連鎖させる関数(未完)
state rensa(state state){
  int i=0;
  int vanish[78];

  for(i=0; i<78; i++){
    vanish[78] = i;

    if(state.banmen[i] == state.banmen[i+1]){
      vanish[78] = i+1;
    }else if(state.banmen[i] == state.banmen[i-1]){
      vanish[78] = i-1;
    }else if(state.banmen[i] == state.banmen[i+6]){
      vanish[78] = i+6;
    }else if(state.banmen[i] == state.banmen[i-6]){
      vanish[78] = i-6;
    }

  }
  


  return state;
}

//探索(未完)
int search_rensa(state search_state){

  int rensa=0,maxrensa=0;


  return 3;
}
